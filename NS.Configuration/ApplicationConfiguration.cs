﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace NS.Configuration
{
    /// <summary>
    /// Application conventions
    /// </summary>
    public class ApplicationConfiguration
    {
        private readonly ICollection<Assembly> _assemblies = new List<Assembly>();

        /// <summary>
        /// Assemblies to be accessible within the installers
        /// </summary>
        public IEnumerable<Assembly> Assemblies
        {
            get { return _assemblies; }
        }

        /// <summary>
        /// Adds an assembly which contains the specified type into the list of assemblies accessible by installers
        /// </summary>
        /// <typeparam name="T">Type contained within the assembly to be added</typeparam>
        /// <returns>ApplicationConfiguration instance</returns>
        public ApplicationConfiguration AddAssemblyContaining<T>()
        {
            var assembly = typeof (T).Assembly;
            if(!_assemblies.Contains(assembly))
                _assemblies.Add(assembly);
            return this;
        }
    }
}
