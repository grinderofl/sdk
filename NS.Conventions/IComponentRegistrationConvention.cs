﻿using Castle.Windsor;

namespace NS.Conventions
{
    /// <summary>
    /// Registers components during application startup
    /// </summary>
    public interface IComponentRegistrationConvention
    {
        /// <summary>
        /// Registers components with container
        /// </summary>
        /// <param name="container">Container to register with</param>
        void Register(IWindsorContainer container);
    }
}