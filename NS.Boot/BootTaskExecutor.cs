﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NS.Configuration;

namespace NS.Boot
{
    /// <summary>
    /// Executes boot tasks
    /// </summary>
    public class BootTaskExecutor
    {
        /// <summary>
        /// Executes the boot tasks
        /// </summary>
        /// <param name="configuration">Application Configuration instance</param>
        /// <param name="container">Component container instance</param>
        public void ExecuteBootTasks(ApplicationConfiguration configuration, IWindsorContainer container)
        {
            var childContainer = new WindsorContainer();
            foreach (var assembly in configuration.Assemblies)
                childContainer.Register(Types.FromAssembly(assembly).BasedOn<IBootTask>().WithServiceDefaultInterfaces());

            foreach (var task in childContainer.ResolveAll<IBootTask>())
                task.Execute(container);
        }
    }
}
