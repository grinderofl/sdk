﻿using Castle.Windsor;

namespace NS.Boot
{
    /// <summary>
    /// Boot task to be executed on application launch
    /// </summary>
    public interface IBootTask
    {
        /// <summary>
        /// Executes a boot task
        /// </summary>
        /// <param name="container">Object containing dependencies</param>
        void Execute(IWindsorContainer container);
    }
}