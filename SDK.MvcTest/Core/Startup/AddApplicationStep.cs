﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NS.Configuration;
using NS.Mvc;

namespace SDK.MvcTest.Core.Startup
{
    public class AddApplicationStep : IStartupStep
    {
        public void Start(ApplicationConfiguration configuration)
        {
            configuration.AddAssemblyContaining<MvcApplication>();
        }
    }
}