﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NS.Configuration;
using NS.EntityFramework.Install;
using NS.Mvc;

namespace SDK.MvcTest.Core.Startup
{
    public class EnableEntityFrameworkStep : IStartupStep
    {
        public void Start(ApplicationConfiguration configuration)
        {
            configuration.AddAssemblyContaining<IEFInstaller>();
        }
    }
}