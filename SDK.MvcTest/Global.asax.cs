﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Routing;
using NS.Mvc;
using SDK.MvcTest.Core.Startup;

namespace SDK.MvcTest
{
    // Note: For instructions on enabling IIS6 or IIS7 classic mode, 
    // visit http://go.microsoft.com/?LinkId=9394801
    public class MvcApplication : ConventionApplication
    {
        //protected void Application_Start()
        //{
        //    AreaRegistration.RegisterAllAreas();

        //    WebApiConfig.Register(GlobalConfiguration.Configuration);
        //    FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
        //    RouteConfig.RegisterRoutes(RouteTable.Routes);
        //}
        protected override void Configure()
        {
            // Order of operations is important to allow overriding defaults
            // Assemblies containing application conventions should be added before SDK ones
            IEnumerable<IStartupStep> steps = new IStartupStep[]
            {
                new AddApplicationStep(),
                new EnableEntityFrameworkStep(), 
            };

            foreach (var startupStep in steps)
            {
                startupStep.Start(Configuration);
            }
        }


    }
}