﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS.Domain.Base;
using NS.Domain.DataAccess;
using NS.EntityFramework.DataAccess;
using NS.Testing;
using NUnit.Framework;

namespace SDK.IntegrationTests.EntityFramework
{
    public class RepositorySpecs
    {
        public abstract class RepositoryContext : ContextSpecification
        {
            protected IRepository Repository;
            protected DbContext DbContext;

            protected override void SharedContext()
            {
                DbContext = new TestContext();
                if (DbContext.Database.Exists())
                    DbContext.Database.Delete();
                DbContext.Database.Create();
                Repository = new EFRepository(DbContext);
            }
        }

        public class WhenSavingNewEntity : RepositoryContext
        {
            protected override void Because()
            {
                Repository.Save(new TestEntity());
            }

            [Test]
            public void should_save()
            {
                DbContext.Set<TestEntity>().Any().ShouldBeTrue();
            }
        }

        public class WhenSavingExistingEntity : RepositoryContext
        {
            protected override void Context()
            {
                Repository.Save(new TestEntity() {Name = "Test"});
            }

            protected override void Because()
            {
                var entity = Repository.Query<TestEntity>().FirstOrDefault(x => x.Name == "Test");
                entity.Name = "Because";
                Repository.Save(entity);
            }

            [Test]
            public void should_update()
            {
                DbContext.Set<TestEntity>().Any(x => x.Name == "Test").ShouldBeFalse();
                DbContext.Set<TestEntity>().Any(x => x.Name == "Because").ShouldBeTrue();
            }
        }
    }

    public class TestContext : DbContext
    {
        public DbSet<TestEntity> TestEntities { get; set; }
    }

    public class TestEntity : EntityWithLongId
    {
        public string Name { get; set; }
    }
}
