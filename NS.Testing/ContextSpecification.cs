﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;

namespace NS.Testing
{
    public abstract class ContextSpecification : TestBase
    {
        [TestFixtureSetUp]
        public void TestFixtureSetUp()
        {
            BeforeAllSpecs();
        }

        [TestFixtureTearDown]
        public void TestFixtureTearDown()
        {
            AfterAllSpecs();
        }

        /// <summary>
        /// Run before tests
        /// </summary>
        public override void SetUp()
        {
            SharedContext();
            Context();
            Because();
        }

        /// <summary>
        /// Run after tests
        /// </summary>
        public override void TearDown()
        {
            BecauseAfter();
            CleanUpContext();
        }

        /// <summary>
        /// Executed before Context and before tests
        /// </summary>
        protected virtual void SharedContext() { }

        /// <summary>
        /// Executed before Because and before tests
        /// </summary>
        protected virtual void Context() { }

        /// <summary>
        /// Executed after BecauseAfter
        /// </summary>
        protected virtual void CleanUpContext() { }

        /// <summary>
        /// Executed before all tests in fixture are run
        /// </summary>
        protected virtual void BeforeAllSpecs() { }

        /// <summary>
        /// Executed after all tests in fixture have ran
        /// </summary>
        protected virtual void AfterAllSpecs() { }

        /// <summary>
        /// Executed before tests
        /// </summary>
        protected virtual void Because() { }

        /// <summary>
        /// Executed after tests
        /// </summary>
        protected virtual void BecauseAfter() { }
    }
}
