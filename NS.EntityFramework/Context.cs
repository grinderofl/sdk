﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NS.EntityFramework
{
    public class Context : DbContext
    {
        public Context(DbCompiledModel model) : base("DefaultConnection", model)
        {
            
        }

        public override int SaveChanges()
        {
            var result = base.SaveChanges();
            return result;
        }
    }
}
