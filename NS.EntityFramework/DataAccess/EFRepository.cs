﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS.Domain.Base;
using NS.Domain.DataAccess;
using NS.Domain.Query;

namespace NS.EntityFramework.DataAccess
{
    public class EFRepository : IRepository
    {
        private readonly DbContext _context;

        public EFRepository(DbContext context)
        {
            _context = context;
        }

        public T Query<T>(QueryObject<T> query) where T : class
        {
            return query.Execute(this);
        }

        public IQueryable<T> Query<T>() where T : class
        {
            return _context.Set<T>();
        }

        public void Save<T>(T entity) where T : class
        {
            var ent = _context.Entry(entity);
            if (ent == null || ent.State == EntityState.Detached)
            {
                _context.Set<T>().Add(entity);
            }
            else
                ent.State = EntityState.Modified;

            _context.SaveChanges();
        }

        public void Remove<T>(T entity) where T : class
        {
            _context.Set<T>().Remove(entity);
            _context.SaveChanges();
        }
    }
}
