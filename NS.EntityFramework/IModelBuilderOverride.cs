﻿using System.Data.Entity.ModelConfiguration;

namespace NS.EntityFramework
{
    public interface IModelBuilderOverride<T> where T : class
    {
        void Configure(EntityTypeConfiguration<T> entity);
    }
}