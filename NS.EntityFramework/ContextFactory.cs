﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Castle.Windsor;

namespace NS.EntityFramework
{
    public class ContextFactory : IDbContextFactory<Context>
    {
        private readonly IWindsorContainer _container;

        public ContextFactory()
        {
            _container = new WindsorContainer();
            //_container.Install(new EFInstaller(typeof(User).Assembly));
        }

        public Context Create()
        {
            return _container.Resolve<DbContext>() as Context;
        }
    }
}
