﻿using System.Collections.Generic;
using System.Linq;
using NS.Domain.Query;

namespace NS.Domain.DataAccess
{
    /// <summary>
    /// Standard repository pattern
    /// </summary>
    public interface IRepository
    {
        /// <summary>
        /// Executes a query object on repository
        /// </summary>
        /// <typeparam name="T">Type to return</typeparam>
        /// <param name="query">Query to be executed</param>
        /// <returns>Executed query</returns>
        T Query<T>(QueryObject<T> query) where T : class;

        ///// <summary>
        ///// Executes a list query object on repository
        ///// </summary>
        ///// <typeparam name="T">Type of collection to return</typeparam>
        ///// <param name="query">Query to execute</param>
        ///// <returns>Executed query</returns>
        //IQueryable<T> Query<T>(ListQueryObject<T> query) where T : class;

        /// <summary>
        /// Queries an entity from database
        /// </summary>
        /// <typeparam name="T">Type of entity to query</typeparam>
        /// <returns>Queried entity</returns>
        IQueryable<T> Query<T>() where T : class;

        /// <summary>
        /// Saves an object to database
        /// </summary>
        /// <typeparam name="T">Type of object to save</typeparam>
        /// <param name="entity">Object to save</param>
        void Save<T>(T entity) where T : class;

        /// <summary>
        /// Removes an object from database
        /// </summary>
        /// <typeparam name="T">Type of object to remove</typeparam>
        /// <param name="entity">Object to remove</param>
        void Remove<T>(T entity) where T : class;
    }
}
