﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NS.Domain.Base
{
    public abstract class EntityWithLongIdAndCreatedDate : EntityWithLongId, IHasCreatedDate
    {
        public DateTime Created { get; set; }
    }
}
