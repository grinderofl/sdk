﻿using System;

namespace NS.Domain.Base
{
    public interface IHasCreatedDate
    {
        DateTime Created { get; set; } 
    }
}