﻿using System;

namespace NS.Domain.Base
{
    public abstract class Entity
    {
        protected Entity()
        {
            Modified = DateTime.Now;
        }
        public DateTime Modified { get; set; }
    }


    public abstract class Entity<TPk> : Entity
    {
        public TPk Id { get; set; }
    }
}
