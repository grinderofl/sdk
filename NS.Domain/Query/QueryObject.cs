﻿using NS.Domain.DataAccess;

namespace NS.Domain.Query
{
    /// <summary>
    /// Query to execute on repository
    /// </summary>
    /// <typeparam name="T">Type to return</typeparam>
    public abstract class QueryObject<T>
    {
        /// <summary>
        /// Executes a single query object
        /// </summary>
        /// <param name="repository">Repository object</param>
        /// <returns>Executed object</returns>
        public abstract T Execute(IRepository repository);
    }
}