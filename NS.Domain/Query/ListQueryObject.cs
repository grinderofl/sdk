﻿using System.Collections.Generic;
using System.Linq;
using NS.Domain.DataAccess;

namespace NS.Domain.Query
{
    /// <summary>
    /// Executes a query list object
    /// </summary>
    /// <typeparam name="T"></typeparam>
    public abstract class ListQueryObject<T>
    {
        public abstract IQueryable<T> Execute(IRepository repository);
    }
}