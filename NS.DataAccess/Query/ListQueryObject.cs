﻿using System.Collections.Generic;

namespace NS.DataAccess.Query
{
    public abstract class ListQueryObject<T>
    {
        public abstract IEnumerable<T> Execute(IRepository repository);
    }
}