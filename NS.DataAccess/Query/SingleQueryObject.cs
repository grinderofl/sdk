﻿namespace NS.DataAccess.Query
{
    public abstract class SingleQueryObject<T>
    {
        public abstract T Execute(IRepository repository);
    }
}