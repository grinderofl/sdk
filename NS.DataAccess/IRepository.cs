﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NS.DataAccess.Query;

namespace NS.DataAccess
{
    public interface IRepository
    {
        T Query<T>(SingleQueryObject<T> query) where T : class;
        ICollection<T> Query<T>(ListQueryObject<T> query) where T : class;
        IQueryable<T> Set<T>() where T : class;
        void Add<T>(T entity) where T : class;
        void Remove<T>(T entity) where T : class;
    }
}
