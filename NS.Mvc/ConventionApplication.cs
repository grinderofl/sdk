﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using Castle.MicroKernel.Registration;
using Castle.Windsor;
using NS.Boot;
using NS.Configuration;

namespace NS.Mvc
{
    /// <summary>
    /// Convention Http Application
    /// </summary>
    public abstract class ConventionApplication : HttpApplication
    {
        private static IWindsorContainer _container;
        private readonly Queue<IWindsorInstaller> _installers = new Queue<IWindsorInstaller>();
        private readonly ApplicationConfiguration _configuration = new ApplicationConfiguration();
        
        /// <summary>
        /// Configures the application on startup
        /// </summary>
        protected abstract void Configure();

        /// <summary>
        /// Gets the dependency injector container
        /// </summary>
        public IWindsorContainer Container
        {
            get { return _container; }
        }

        /// <summary>
        /// Gets the application configuration
        /// </summary>
        public ApplicationConfiguration Configuration
        {
            get { return _configuration; }
        }

        /// <summary>
        /// Executes on application startup
        /// </summary>
        protected virtual void Application_Start()
        {
            _container = CreateContainer();
            Configure();
            InstallWindsorInstallers();
            ExecuteBootTasks();
        }

        /// <summary>
        /// Registers installer with the container
        /// </summary>
        /// <typeparam name="T">Type to register</typeparam>
        protected void RegisterInstaller<T>() where T : IWindsorInstaller, new()
        {
            RegisterInstaller(new T());
        }

        /// <summary>
        /// Registers installer with the container
        /// </summary>
        /// <param name="windsorInstaller">Installer to register</param>
        protected void RegisterInstaller(IWindsorInstaller windsorInstaller)
        {
            _installers.Enqueue(windsorInstaller);
        }

        /// <summary>
        /// Creates the container
        /// </summary>
        /// <returns></returns>
        protected virtual IWindsorContainer CreateContainer()
        {
            return new WindsorContainer();
        }

        #region Private methods

        private void InstallWindsorInstallers()
        {
            foreach (var installer in _installers)
                _container.Install(installer);
        }

        private void ExecuteBootTasks()
        {
            var executor = new BootTaskExecutor();
            executor.ExecuteBootTasks(Configuration, Container);
        }

        #endregion

    }
}
