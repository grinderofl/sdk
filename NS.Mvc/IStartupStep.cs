﻿using NS.Configuration;

namespace NS.Mvc
{
    public interface IStartupStep
    {
        void Start(ApplicationConfiguration configuration);
    }
}